const Home = () => import('@/pages/Home.vue')
const Register = () => import('@/pages/Register.vue')
const CreateTicket = () => import('@/pages/CreateTicket.vue')
const TicketDetail = () => import('@/pages/TicketDetail.vue')
const TicketDownload = () => import('@/pages/TicketDownload.vue')
const Index = () => import('@/pages/Index.vue')

export default [
  {
    path: '/create-ticket/:companyId/:packageId/:variantId',
    name: 'CreateTicket',
    component: CreateTicket,
  },
  {
    path: '/ticket/:companyId',
    name: 'Home',
    component: Home,
  },
  {
    path: '/ticket-detail/:id',
    name: 'ticket-detail',
    component: TicketDetail,
  },
  {
    path: '/ticket-download/:id',
    name: 'ticket-download',
    component: TicketDownload,
  },
  {
    path: '/register/:id',
    name: 'register',
    component: Register,
  },
  {
    path: '/',
    name: 'index',
    component: Index,
  },
]