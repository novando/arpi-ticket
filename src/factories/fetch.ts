export default {
  getData (url:string) {
    return fetch(url, {
      method: 'GET',
      headers: {
        'content-Type': 'application/json'
      }
    })
      .then((res) => {
        if (res.status < 200 && res.status >= 400) {
          throw res.statusText
        }
        return res.json()
      })
      .then((res) => {
        return res
      })
      .catch((err) => {
        throw err
      })
  },
  postData (url:string, payload:any) {
    return fetch(url, {
      method: 'POST',
      headers: {
        'content-Type': 'application/json'
      },
      body: JSON.stringify(payload),
    })
      .then((res) => {
        if (res.status < 200 && res.status >= 400) {
          throw res.statusText
        }
        return res.json()
      })
      .then((res) => {
        return res
      })
      .catch((err) => {
        throw err
      })
  },
}