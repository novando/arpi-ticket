const timezone = +7

const timestampToDate = (val:string) => {
  const timeStr = (val.split('T')[1]).split('.')[0]
  const dateStr = (val.split('T')[0])
  const date = new Date(`${dateStr} ${timeStr}`)
  date.setHours(date.getHours() + timezone)
  return date
}

export default {
  currency (val:number) {
    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    }).format(val)
  },
  toDate (val:string) {
    if (val === '') return
    const date = timestampToDate(val)
    const months = [
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
    ]
    return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`
  },
  toTime (val:string) {
    if (val === '') return
    const date = timestampToDate(val)
    const hour = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours()
    const minute = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
    return `${hour}:${minute}`
  }
}