import one from '@/assets/logos/1.jpg'
import two from '@/assets/logos/2.jpg'
import three from '@/assets/logos/3.jpg'

const logos = [one, two, three]

const genLogo = (index:number) => {
  return logos[index - 1]
}

export default genLogo