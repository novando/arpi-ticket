import fetch from '@/factories/fetch'

const apiUrl = `${import.meta.env.VITE_API_SITE}`

export default {
  getPackages (id:string) {
    return fetch.getData(`${apiUrl}/package/${id}`)
  },
  getVariants (comapnyId:string, packageId:string) {
    return fetch.getData(`${apiUrl}/variant/${comapnyId}/${packageId}`)
  },
  createTicket (payload:any) {
    return fetch.postData(`${apiUrl}/pax/add`, payload)
  },
  register (id:string, payload:any) {
    return fetch.putData(`${apiUrl}/pax/register/${id}`, payload)
  },
  getTicket (id:string) {
    return fetch.getData(`${apiUrl}/ticket/${id}`)
  },
}