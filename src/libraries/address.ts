import fetchApi from '@/factories/fetch'

const apiAddress = `${import.meta.env.VITE_API_ADDRESS}`

export default {
  getProvinces () {
    return fetchApi.getData(`${apiAddress}/provinces.json`)
  },
  getCities (provinceId:string) {
    return fetchApi.getData(`${apiAddress}/regencies/${provinceId}.json`)
  },
}