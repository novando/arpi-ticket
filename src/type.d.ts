declare interface packageDataType {
  id: number,
  name: string,
}
declare interface packageType {
  company_name: string,
  package_data: packageDataType[],
}
declare interface variantDataType {
  id: number,
  price: number,
  name: string,
  max_capacity: number,
  duration: number
}
declare interface provinceDataType {
  id: string,
  name: string,
}
declare interface cityDataType {
  id: string,
  province_id: string,
  name: string,
}